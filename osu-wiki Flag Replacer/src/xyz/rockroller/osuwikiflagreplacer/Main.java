package xyz.rockroller.osuwikiflagreplacer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main{
	public static void main(String[] args) throws IOException{
		iterateFolder(Paths.get("E:\\Github Gitlab\\osu-wiki\\wiki"));
		iterateFolder(Paths.get("E:\\Github Gitlab\\osu-wiki\\news"));
	}

	public static void iterateFolder(Path folder) throws IOException{
		try(Stream<Path> pathStream = Files.find(folder, Integer.MAX_VALUE, (path, attributes)->!Files.isDirectory(path))){
			pathStream.forEach(p->{
				if(p.getFileName().toString().endsWith(".md")){
					System.out.println(p.getFileName());
					try{
						replaceInlineFlag(p);
						replaceFlagReferenceWithTranslation(p);
						replaceFlagReferenceWithoutTranslation(p);
						stripMultiNewline(p);
					}catch(IOException e){
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
	}

	public static void stripMultiNewline(Path path) throws IOException{
		Files.writeString(path, new String(Files.readAllBytes(path), StandardCharsets.UTF_8).replaceAll("[\\n\\r]{2,}$", "\r\n"));
	}

	public static void replaceInlineFlag(Path path) throws IOException{
		final String regex = "\\!\\s*\\[\\]\\s*\\[flag_\\S{2}\\]";
		final String input = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
		String output = input;

		final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		final Matcher matcher = pattern.matcher(input);

		while(matcher.find()){
			for(int i = 1; i <= matcher.groupCount(); i++){
				output = output.replaceAll(Pattern.quote(matcher.group(0)), String.format("::{ flag=\"%s\" }::", matcher.group(1)));
			}
		}
		Files.writeString(path, output);
	}

	public static void replaceFlagReferenceWithoutTranslation(Path path) throws IOException{
		final String regex = "\\[flag_.{2}\\]: /wiki/shared/flag/.{2}\\.gif";
		final String input = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
		String output = input;

		final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		final Matcher matcher = pattern.matcher(input);

		while(matcher.find()){
			output = output.replaceAll(Pattern.quote(matcher.group(0)), "");
		}
		Files.writeString(path, output);
	}
	
	public static void replaceFlagReferenceWithTranslation(Path path) throws IOException{
		final String regex = "\\[flag_\\S{2}\\]: /wiki/shared/flag/\\S{2}.gif \\\".*\\\"";
		final String input = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
		String output = input;

		final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		final Matcher matcher = pattern.matcher(input);

		while(matcher.find()){
			output = output.replaceAll(Pattern.quote(matcher.group(0)), "");
		}
		Files.writeString(path, output);
	}
}
